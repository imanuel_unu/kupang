Rails.application.routes.draw do
  get 'welcome/index'
  
  resources :articles do
  	resources :comments
  
  root 'articles#index'

end
end
